package fr.uha.sakhi.javatar.agiles.évènement;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class BusEvenements implements Observable{


    private Map<TypeEvenements,Set<Observateur>> observations=new HashMap<>();


    /**
     * abonner un objet a une liste de types d'evenements*/
    @Override
    public void abonner(Observateur observateur, TypeEvenements type, TypeEvenements... types) {
        mettreAJourAbonnement( observateur, type);
        for (TypeEvenements t:types)
        {
            mettreAJourAbonnement( observateur, t);
        }

    }

    /**
     * mettre a jour l#'abonnement d'un objet
     */
    private void mettreAJourAbonnement(Observateur observateur, TypeEvenements type) {
        observations.compute(type, (typeEvenements, observateurs) -> {
            if (observateurs==null)
            {
                observateurs=new HashSet<>();
            }
            observateurs.add(observateur);
            return observateurs;
        });
    }

    /**
     * desabonner un objet
     * @return void
     */
    @Override
    public void desabonner(Observateur o) {
      Collection<Set<Observateur>> ensembles=observations.values();
      for (Set<Observateur> ensemble:ensembles)
      {
          if (ensemble.contains(o))
          {
              ensemble.remove(o);
          }
      }
    }

    /**
     * notifier les observateurs
     * @return void
     */
    @Override
    public void notifierObservateurs(Evenement evenement) {

        TypeEvenements type=TypeEvenements.valueOf(evenement.getClass().getSimpleName());
        if (observations.size()==0)
            return;
        if (observations.get(type)==null)
            return;
        for (Observateur obs:observations.get(type))
        {
            Method method= trouvermethode(obs.getClass(),type);
            appelerMethode(method,obs,evenement);
        }
    }


    /**
     * executer une methode d'un observateur
     * @return void
     */
    private void appelerMethode(Method method, Observateur obs, Evenement evenement)
    {
        try {
            method.invoke(obs,evenement);
        }
        catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException ignored) { }
        catch (NullPointerException e) {
            System.out.println("erreur au niveau des annotaions");
        }
    }
    /**
     * trouver une methode d'un observateur
     * @return void
     */
    private static Method trouvermethode(final Class<?> cl, TypeEvenements t) {

        final List<Method> methode = new ArrayList<>(Arrays.asList(cl.getDeclaredMethods()));
        for (final Method method : methode) {
            if (method.isAnnotationPresent(Souscrire.class)) {
                Souscrire annotInstance = method.getAnnotation(Souscrire.class);
                if (annotInstance.TypeEvenement().equals(t)) {
                    return method;
                }
            }
        }
        return null;
    }
    }




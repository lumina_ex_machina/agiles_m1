package fr.uha.sakhi.javatar.agiles.évènement;


import fr.uha.sakhi.javatar.agiles.données.Utilisateurs.Utilisateur;
import javafx.beans.property.BooleanProperty;

/*
 * Event bus events used in Dashboard are listed here as inner classes.
 */
public abstract class LesEvenements {

    public static final class EvenementLogin  implements Evenement{
        private final String pseudo, motdepasse;
        public EvenementLogin(final String pseudo, final String motdepasse) {
            this.pseudo = pseudo;
            this.motdepasse = motdepasse;
        }
        public String getPseudo() {
            return pseudo;
        }

        public String getMotdepasse() {
            return motdepasse;
        }
    }

    public static final class ReactionEvenementLogin  implements Evenement{
        private   Utilisateur utilisateur;
        private  String message;

        public ReactionEvenementLogin(Utilisateur utilisateur, String message) {
            this.utilisateur = utilisateur;
            this.message = message;
        }

        public Utilisateur getUtilisateur() {
            return utilisateur;
        }

        public void setUtilisateur(Utilisateur utilisateur) {
            this.utilisateur = utilisateur;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

}

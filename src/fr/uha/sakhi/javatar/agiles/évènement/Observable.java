package fr.uha.sakhi.javatar.agiles.évènement;

public interface Observable<O extends Observateur,E extends Evenement>{

    void abonner(O observateur,TypeEvenements type,TypeEvenements ... types);

    void desabonner(O observateur);

    void notifierObservateurs(E evenement);
}

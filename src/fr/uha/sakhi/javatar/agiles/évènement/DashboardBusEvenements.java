package fr.uha.sakhi.javatar.agiles.évènement;

import fr.uha.sakhi.javatar.agiles.FenetrePrincipale;

public class DashboardBusEvenements {


    private final BusEvenements busEvenements=new BusEvenements();
    public static void abonner(Observateur observateur, TypeEvenements type, TypeEvenements ... types)
    {
        FenetrePrincipale.getDashboardBusEvenements().busEvenements.abonner(observateur,type,types);
    }

    public static void desabonner(Observateur o) {
        FenetrePrincipale.getDashboardBusEvenements().busEvenements.desabonner(o);
    }

    public static void notifierObservateurs(Evenement e) {
        FenetrePrincipale.getDashboardBusEvenements().busEvenements.notifierObservateurs(e);
    }




}
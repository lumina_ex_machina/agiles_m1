package fr.uha.sakhi.javatar.agiles.évènement;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Souscrire {
  TypeEvenements TypeEvenement();
}

package fr.uha.sakhi.javatar.agiles.données.annotations;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Autoriser {
    boolean Administrateur() default true;
    boolean Etudiant() default false;
    boolean Enseignant() default false;
}


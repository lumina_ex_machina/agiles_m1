package fr.uha.sakhi.javatar.agiles.données.annotations;

import fr.uha.sakhi.javatar.agiles.données.TypesColonnesTables;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Colonne {
    String nom()  ;
    String nomDAffichage()  ;
    boolean Affichable() default false;
    boolean Modifiable() default false;
    TypesColonnesTables type();
}

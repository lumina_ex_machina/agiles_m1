package fr.uha.sakhi.javatar.agiles.données.Utilisateurs;

import fr.uha.sakhi.javatar.agiles.données.annotations.Colonne;
import fr.uha.sakhi.javatar.agiles.données.annotations.Table;

import static fr.uha.sakhi.javatar.agiles.données.TypesColonnesTables.*;

@Table(nom="Utilisateur",nomAffichage = "Utilisateurs",DAO = UtilisateurDAO.class)
public  class Utilisateur {
    /**
     *   l'utilisateur courant
     */
    public static Utilisateur utilisateurCourant=null;
    /**
     *   le nom de l'utilisateur
     */
    @Colonne(nom="nom",nomDAffichage ="Nom",Affichable = true,Modifiable = true,type = TEXT)
    public String nom;

    /**
     *   le prenom de l'utilisateur
     */
    @Colonne(nom="prenom",nomDAffichage ="Prenom",Affichable = true,Modifiable = true,type = TEXT)
    public String prenom;
    @Colonne(nom="code",nomDAffichage ="Code",Affichable = true,Modifiable = true,type = TEXT)
    /**
     *   le code de l'utilisateur
     */
    public String code;
    @Colonne(nom="role",nomDAffichage ="Role",Affichable = true,Modifiable = true,type = TEXT)
    public String role;
    /**
     *   le mot de passe  de l'utilisateur
     */
    @Colonne(nom="motdepasse",nomDAffichage ="Mot de Passe",Affichable = false,Modifiable = true,type = TEXT)
    public String motdePasse;
    /**
     * Constructeur
     * @param nom le nom de l'utilisateur
     * @param prenom le prenom de l'utilisateur
     * @param code le code de l'utilisateur
     * @param motdePasse le mot de passe de l'utilisateur
     */
    public Utilisateur(String nom, String prenom, String code, String role, String motdePasse) {
        this.nom = nom;
        this.prenom = prenom;
        this.code = code;
        this.role = role;
        this.motdePasse = motdePasse;
    }

    /**
     * Constructeur
     * @param nom le nom de l'utilisateur
     * @param prenom le prenom de l'utilisateur
     * @param code le code de l'utilisateur
     */
    public Utilisateur(String nom, String prenom, String code, String role) {
        this.nom = nom;
        this.prenom = prenom;
        this.code = code;
        this.role = role;
    }

    /**
     * Constructeur
     */
    public Utilisateur() {
    }

    /**
     * getter pour l'utilisateur courant
     * @return l'utilisateur courant
     */
    public static Utilisateur getUtilisateurCourant() {
        return utilisateurCourant;
    }

    /**
     * setter pour l'utilisateur courant
     * @return void
     */
    public static void setUtilisateurCourant(Utilisateur utilisateurCourant) {
        Utilisateur.utilisateurCourant = utilisateurCourant;
    }


    /**
     * getter pour le nom de l'utilisateur
     * @return le nom de l'utilisateur
     */
    public String getNom() {
        return nom;
    }

    /**
     * setter pour le nom de l'utilisateur
     * @return void
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * getter pour le prenomnom de l'utilisateur
     * @return le prenomnom de l'utilisateur
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * setter pour le prenom de l'utilisateur
     * @return void
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * getter pour le code de l'utilisateur
     * @return le code de l'utilisateur
     */
    public String getCode() {
        return code;
    }

    /**
     * setter pour le code de l'utilisateur
     * @return void
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * getter pour le role de l'utilisateur
     * @return le role de l'utilisateur
     */
    public String getRole() {
        return role;
    }

    /**
     * setter pour le role de l'utilisateur
     * @return void
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * getter pour le mot de passe de l'utilisateur
     * @return le mot de passe de l'utilisateur
     */
    public String getMotdePasse() {
        return motdePasse;
    }

    /**
     * setter pour le mot de passe de l'utilisateur
     * @return void
     */
    public void setMotdePasse(String motdePasse) {
        this.motdePasse = motdePasse;
    }
}

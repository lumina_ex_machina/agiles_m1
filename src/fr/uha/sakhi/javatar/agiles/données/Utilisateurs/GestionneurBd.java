package fr.uha.sakhi.javatar.agiles.données.Utilisateurs;

import fr.uha.sakhi.javatar.agiles.données.annotations.Autoriser;

import java.lang.reflect.Method;
import java.util.List;

public abstract class GestionneurBd  {

    final static String URL = "jdbc:mysql://localhost:3306/agiles";
    final static String LOGIN="root";
    final static String PASS="retro6";

    protected void gererDroits(String nomMethode) throws IllegalAccessException {
        try {
            Method method=getClass().getDeclaredMethod(nomMethode,this.getClass());
            if (method.isAnnotationPresent(Autoriser.class)) {
                Autoriser annotInstance = method.getAnnotation(Autoriser.class);
                if (Utilisateur.getUtilisateurCourant()==null)
                    throw new IllegalAccessException();
                if (Utilisateur.utilisateurCourant.getRole().equals(role.Etudiant))
                {
                    if (!annotInstance.Etudiant())
                    {
                        throw new IllegalAccessException();
                    }
                }
                if (Utilisateur.utilisateurCourant.getRole().equals(role.Enseignant))
                {
                    if (!annotInstance.Enseignant())
                    {
                        throw new IllegalAccessException();
                    }
                }
                if (Utilisateur.utilisateurCourant.getRole().equals(role.Administrateur))
                {
                    if (!annotInstance.Administrateur())
                    {
                        throw new IllegalAccessException();
                    }
                }
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

    }
}

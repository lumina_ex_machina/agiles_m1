package fr.uha.sakhi.javatar.agiles.données.Utilisateurs;

import fr.uha.sakhi.javatar.agiles.données.annotations.*;
import fr.uha.sakhi.javatar.agiles.évènement.DashboardBusEvenements;
import fr.uha.sakhi.javatar.agiles.évènement.LesEvenements;
import fr.uha.sakhi.javatar.agiles.évènement.Observateur;
import fr.uha.sakhi.javatar.agiles.évènement.Souscrire;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;

import static fr.uha.sakhi.javatar.agiles.évènement.TypeEvenements.EvenementLogin;


public class UtilisateurDAO extends GestionneurBd implements Observateur{
    /**
     *   l'instance statique du DAO des utilisateur
     */
    private  static UtilisateurDAO instance = new UtilisateurDAO();


    /**
     * Constructeur
     */
    public UtilisateurDAO()
    {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    Class.forName("oracle.jdbc.OracleDriver");
                } catch (ClassNotFoundException e2) {
                    System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
                }
                abonnerEvenementLogin();
            }
        });
    }

    /**
     * abonner cette classe aux evenements de login
     * @return void
     */
    private void abonnerEvenementLogin() {
        DashboardBusEvenements.abonner(this, EvenementLogin);
    }

    /**
     * code a executer au declenchement des evenements de login
     * @return void
     */
    @Souscrire(TypeEvenement = EvenementLogin)
    public void evenementLogin(final LesEvenements.EvenementLogin e)
    {
        Utilisateur utilisateur=authentifier(e.getPseudo(),e.getMotdepasse());
        Utilisateur.utilisateurCourant=utilisateur;
        String message="";
        if (utilisateur==null)
        {
             message="erreur d'authentification";
        }
        else
        {
            message="authentification";
        }
        DashboardBusEvenements.notifierObservateurs(new LesEvenements.ReactionEvenementLogin(utilisateur,message));

    }

    /**
     * ajout d'un nouveau utilisateur
     * @return liste vide
     */
    @Autoriser(Administrateur = true,Enseignant = false,Etudiant = false)
    @Ajouter
    public ObservableList<Utilisateur> ajouter(Utilisateur utilisateur) throws IllegalAccessException {
        gererDroits("ajouter");
        Connection con = null;
        PreparedStatement ps = null;
        ObservableList<Utilisateur> retour= FXCollections.observableArrayList();

        try {
            con = DriverManager.getConnection(URL, LOGIN, PASS);
            ps = con.prepareStatement("INSERT INTO Utilisateur (nom, prenom, code,role) VALUES (?, ?, ?, ?)");
            ps.setString(1,utilisateur.getNom());
            ps.setString(2,utilisateur.getPrenom());
            ps.setString(3,utilisateur.getCode());
            ps.setString(4,utilisateur.getRole());


            ps.executeUpdate();


        } catch (Exception ee) {
            ee.printStackTrace();
        } finally {
            //fermeture du preparedStatement et de la connexion
            try {if (ps != null)ps.close();} catch (Exception t) {}
            try {if (con != null)con.close();} catch (Exception t) {}
        }
        return retour;

    }

    /**
     * modification d'un utilisateur
     * @return liste vide
     */
    @Autoriser(Administrateur = true,Enseignant = true,Etudiant = true)
    @Modifier
    public ObservableList<Utilisateur> modifier(Utilisateur utilisateur) throws IllegalAccessException {
        //gererDroits("modifier");
        Connection con = null;
        PreparedStatement ps = null;
        ObservableList<Utilisateur> retour= FXCollections.observableArrayList();

        try {
            con = DriverManager.getConnection(URL, LOGIN, PASS);
            ps = con.prepareStatement("UPDATE Utilisateur SET nom=?,prenom=?,code=?,role=?,motdepasse=? WHERE code=?");
            ps.setString(1,utilisateur.getNom());
            ps.setString(2,utilisateur.getPrenom());
            ps.setString(3,utilisateur.getCode());
            ps.setString(4,utilisateur.getRole());
            ps.setString(5,utilisateur.getMotdePasse());
            ps.setString(6,utilisateur.getCode());


            ps.executeUpdate();


        } catch (Exception ee) {
            ee.printStackTrace();
        } finally {
            //fermeture du preparedStatement et de la connexion
            try {if (ps != null)ps.close();} catch (Exception t) {}
            try {if (con != null)con.close();} catch (Exception t) {}
        }
        return retour;

    }

    /**
     * recuperation d'un utilisateur
     * @return utilisateur/null
     */
    @Autoriser(Administrateur = true,Enseignant = true,Etudiant = true)
    @Chercher(filtre="code")
    public ObservableList<Utilisateur> getUtilisateur(String code) throws IllegalAccessException {
        gererDroits("getUtilisateur");
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs=null;
        ObservableList<Utilisateur> retour= FXCollections.observableArrayList();

        //connexion � la base de donn�es
        try {

            con = DriverManager.getConnection(URL, LOGIN, PASS);
            ps = con.prepareStatement("SELECT * FROM utilisateur WHERE code = ?");
            ps.setString(1,code);

            //on ex�cute la requ�te
            //rs contient un pointeur situ� jusute avant la premi�re ligne retourn�e
            rs=ps.executeQuery();
            //passe � la premi�re (et unique) ligne retourn�e
            if(rs.next())
                retour.add(new Utilisateur(rs.getString("nom"),rs.getString("prenom"),rs.getString("code"),rs.getString("role"),rs.getString("motdepasse")));

        } catch (Exception ee) {
            ee.printStackTrace();
        } finally {
            //fermeture du ResultSet, du PreparedStatement et de la Connection
            try {if (rs != null)rs.close();} catch (Exception t) {}
            try {if (ps != null)ps.close();} catch (Exception t) {}
            try {if (con != null)con.close();} catch (Exception t) {}
        }
        return retour;

    }

    /**
     * authentification des utilisateurs
     * @return utilisateur/null
     */
    public Utilisateur authentifier(String code,String motDePasse)
    {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs=null;
        Utilisateur retour=null;

        //connexion � la base de donn�es
        try {

            con = DriverManager.getConnection(URL, LOGIN, PASS);
            ps = con.prepareStatement("SELECT * FROM utilisateur WHERE code = ? AND motdepasse= ?");
            ps.setString(1,code);
            ps.setString(2,motDePasse);

            //on ex�cute la requ�te
            //rs contient un pointeur situ� jusute avant la premi�re ligne retourn�e
            rs=ps.executeQuery();
            //passe � la premi�re (et unique) ligne retourn�e
            if(rs.next())
                retour=new Utilisateur(rs.getString("nom"),rs.getString("prenom"),rs.getString("code"),rs.getString("role"),rs.getString("motdepasse"));


        } catch (Exception ee) {
            ee.printStackTrace();
        } finally {
            //fermeture du ResultSet, du PreparedStatement et de la Connection
            try {if (rs != null)rs.close();} catch (Exception t) {}
            try {if (ps != null)ps.close();} catch (Exception t) {}
            try {if (con != null)con.close();} catch (Exception t) {}
        }
        return retour;
    }



    /**
     * authentification des utilisateurs
     * @return liste vide
     */
    @Lister
    @Autoriser(Administrateur = true,Enseignant = true,Etudiant = true)
    public ObservableList<Utilisateur> getListeutilisateurs() throws IllegalAccessException {
        //gererDroits("getListeutilisateurs");
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs=null;
        ObservableList<Utilisateur> retour= FXCollections.observableArrayList();

        //connexion � la base de donn�es
        try {

            con = DriverManager.getConnection(URL, LOGIN, PASS);
            ps = con.prepareStatement("SELECT * FROM utilisateur");

            rs=ps.executeQuery();
            while(rs.next())
                retour.add(new Utilisateur(rs.getString("nom"),rs.getString("prenom"),rs.getString("code"),rs.getString("role"),rs.getString("motdepasse")));


        } catch (Exception ee) {
            ee.printStackTrace();
        } finally {
            //fermeture du rs, du preparedStatement et de la connexion
            try {if (rs != null)rs.close();} catch (Exception t) {}
            try {if (ps != null)ps.close();} catch (Exception t) {}
            try {if (con != null)con.close();} catch (Exception t) {}
        }
        return retour;

    }

    /**
     * recuperer l'instance courante du DAO
     * @return instnce courante
     */
    @Instance
    public  static UtilisateurDAO getInstance()
    {
        if (instance==null)
        {
            instance=new UtilisateurDAO();
            return instance;
        }
        return instance;
    }


}

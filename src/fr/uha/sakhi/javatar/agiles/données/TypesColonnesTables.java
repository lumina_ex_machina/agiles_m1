package fr.uha.sakhi.javatar.agiles.données;

public enum TypesColonnesTables {
    TEXT,
    NOMBRE,
    DATE,
    HEURE
}

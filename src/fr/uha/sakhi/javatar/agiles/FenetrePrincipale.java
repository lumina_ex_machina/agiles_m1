package fr.uha.sakhi.javatar.agiles;

import fr.uha.sakhi.javatar.agiles.composantsGraphiques.Tables;
import fr.uha.sakhi.javatar.agiles.données.Utilisateurs.Utilisateur;
import fr.uha.sakhi.javatar.agiles.données.Utilisateurs.UtilisateurDAO;
import fr.uha.sakhi.javatar.agiles.vues.login.VueLogin;
import fr.uha.sakhi.javatar.agiles.évènement.*;
import javafx.application.Platform;
import javafx.scene.Scene;

import javafx.stage.Stage;

import static fr.uha.sakhi.javatar.agiles.évènement.TypeEvenements.ReactionEvenementLogin;


public final class FenetrePrincipale extends Stage implements Observateur {
    private static DashboardBusEvenements dashboardbusEvenements = new DashboardBusEvenements();

    public FenetrePrincipale() {
        Platform.runLater(()->{
            UtilisateurDAO.getInstance();
            abonnerReactionEvenementLogin();
            ajouterEcouteurs();
        });
        mettreAjourContenu();
    }


    @Souscrire(TypeEvenement = ReactionEvenementLogin)
    public void reactionEvenementLogin(final LesEvenements.ReactionEvenementLogin e)
    {
        Platform.runLater(this::mettreAjourContenu);
    }

    public  static DashboardBusEvenements getDashboardBusEvenements() {
        return dashboardbusEvenements;
    }



    private void abonnerReactionEvenementLogin() {
        DashboardBusEvenements.abonner(this, ReactionEvenementLogin);
    }


    private void ajouterEcouteurs() {
       this.focusedProperty().addListener(((observable, ancienneValeur, nouvelleValeur) -> mettreAjourContenu()));
    }


    private void mettreAjourContenu() {

        if (Utilisateur.utilisateurCourant!= null) {

            if (getScene()==null || getScene().getRoot()==null || getScene().getRoot() instanceof VueLogin)
            {
               // setScene(new Scene(new VuePrincipale()));
                setScene(new Scene(new Tables<Utilisateur>(Utilisateur::new)));
            }

        } else {
            if (getScene()==null || getScene().getRoot()==null || getScene().getRoot() instanceof Tables)
            {
                  setScene(new Scene(new VueLogin()));
            }
        }
    }

}

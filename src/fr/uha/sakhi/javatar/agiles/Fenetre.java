package fr.uha.sakhi.javatar.agiles;

import fr.uha.sakhi.javatar.agiles.données.Utilisateurs.Utilisateur;
import fr.uha.sakhi.javatar.agiles.vues.login.VueLogin;
import fr.uha.sakhi.javatar.agiles.vues.VuePrincipale;
import fr.uha.sakhi.javatar.agiles.évènement.*;
import javafx.application.Platform;
import javafx.scene.Scene;

import javafx.stage.Stage;

import static fr.uha.sakhi.javatar.agiles.évènement.TypeEvenements.EvenementLogin;


public final class Fenetre extends Stage implements Observateur {
    private static DashboardBusEvenements dashboardbusEvenements = new DashboardBusEvenements();


    public Fenetre() {
        Platform.runLater(()->{
            abonnerEvenementLogin();
            ajouterEcouteurs();
        });
        mettreAjourContenu();
    }


    @Souscrire(TypeEvenement = EvenementLogin)
    public void evenementLogin(final LesEvenements.EvenementLogin e)
    {
        mettreAjourContenu();
    }

    public  static DashboardBusEvenements getDashboardBusEvenements() {
        return dashboardbusEvenements;
    }



    private void abonnerEvenementLogin() {
        DashboardBusEvenements.abonner(this, EvenementLogin);
    }


    private void ajouterEcouteurs() {
       this.focusedProperty().addListener(((observable, ancienneValeur, nouvelleValeur) -> mettreAjourContenu()));
    }


    private void mettreAjourContenu() {

        if (Utilisateur.utilisateurCourant!= null) {
            setScene(new Scene(new VuePrincipale()));

        } else {
            setScene(new Scene(new VueLogin()));
        }
    }

}

package fr.uha.sakhi.javatar.agiles.vues.login;


import fr.uha.sakhi.javatar.agiles.données.Utilisateurs.Utilisateur;
import fr.uha.sakhi.javatar.agiles.évènement.DashboardBusEvenements;
import fr.uha.sakhi.javatar.agiles.évènement.LesEvenements;
import fr.uha.sakhi.javatar.agiles.évènement.Observateur;
import fr.uha.sakhi.javatar.agiles.évènement.Souscrire;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Pair;

import static fr.uha.sakhi.javatar.agiles.évènement.TypeEvenements.EvenementLogin;
import static fr.uha.sakhi.javatar.agiles.évènement.TypeEvenements.ReactionEvenementLogin;

public class VueLogin extends BorderPane implements Observateur{
    private TextField codeT;
    private PasswordField motDePasseT;
    Label message;
    Button login;
    public VueLogin() {
        Node formulaireLogin = creerFormulaireLogin();
        getChildren().add(formulaireLogin);
        abonnerReactionEvenementLogin();
    }
    private void abonnerReactionEvenementLogin() {
        DashboardBusEvenements.abonner(this, ReactionEvenementLogin);
    }


    private Node creerFormulaireLogin() {
        BorderPane root = new BorderPane();
        HBox entete = new HBox();
        entete.setPadding(new Insets(20,20,20,30));

        GridPane panneau = new GridPane();
        panneau.setPadding(new Insets(20,20,20,20));
        panneau.setHgap(5);
        panneau.setVgap(5);

        Label code = new Label("Code");
        codeT = new TextField();
        Label motDePasse = new Label("mot de passe");
        motDePasseT = new PasswordField();
        login = new Button("Login");
        message = new Label();

        panneau.add(code, 0, 0);
        panneau.add(codeT, 1, 0);
        panneau.add(motDePasse, 0, 1);
        panneau.add(motDePasseT, 1, 1);
        panneau.add(login, 2, 1);
        panneau.add(message, 1, 2);




        Text text = new Text("UHA Courses Planning");
        entete.getChildren().add(text);

        /*
        login.setStyle(" -fx-background-color: \n" +
                "        linear-gradient(#ffd65b, #e68400),\n" +
                "        linear-gradient(#ffef84, #f2ba44),\n" +
                "        linear-gradient(#ffea6a, #efaa22),\n" +
                "        linear-gradient(#ffe657 0%, #f8c202 50%, #eea10b 100%),\n" +
                "        linear-gradient(from 0% 0% to 15% 50%, rgba(255,255,255,0.9), rgba(255,255,255,0));\n" +
                "    -fx-background-radius: 30;\n" +
                "    -fx-background-insets: 0,1,2,3,0;\n" +
                "    -fx-text-fill: #654b00;\n" +
                "    -fx-font-weight: bold;\n" +
                "    -fx-font-size: 14px;\n" +
                "    -fx-padding: 10 20 10 20;");
        */
        text.setStyle("-fx-font-family: fantasy;-fx-font-weight: bold;-fx-font-size: 35;-fx-fill: linear-gradient(deepskyblue,darkblue)");
        panneau.setStyle("" +
              "-fx-background-color:  linear-gradient(rebeccapurple, darkblue);\n" +
              " -fx-border-color: white;\n" +
              " -fx-border-radius: 20;\n" +
              " -fx-padding: 10 10 10 10;\n" +
              " -fx-background-radius: 20;-fx-background-color:  linear-gradient(lightblue, deepskyblue,darkblue);\n" +
              " -fx-border-color: white;\n" +
              " -fx-border-radius: 20;\n" +
              " -fx-padding: 10 10 10 10;\n" +
              " -fx-background-radius: 40");

        login.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                DashboardBusEvenements.notifierObservateurs(new LesEvenements.EvenementLogin(codeT.getText(),motDePasseT.getText()));
            }
        });
        panneau.autosize();
        root.setTop(entete);
        root.setCenter(panneau);
        root.autosize();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                root.setPadding(new Insets(root.getScene().getWindow().getHeight()/2-root.getHeight()/2,root.getScene().getWindow().getWidth()/2+root.getWidth()/2,root.getScene().getWindow().getHeight()/2+root.getHeight()/2,root.getScene().getWindow().getWidth()/2-root.getWidth()/2));
                root.getScene().getWindow().widthProperty().addListener(new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                     if (root.getScene()!=null && root.getScene().getWindow()!=null)
                     {
                         root.setPadding(new Insets(root.getScene().getWindow().getHeight()/2-root.getHeight()/2,root.getScene().getWindow().getWidth()/2+root.getWidth()/2,root.getScene().getWindow().getHeight()/2+root.getHeight()/2,root.getScene().getWindow().getWidth()/2-root.getWidth()/2));

                     }
                    }
                });

            }
        });
        return root;
    }
    @Souscrire(TypeEvenement = ReactionEvenementLogin)
    public void reactionEvenementLogin(final LesEvenements.ReactionEvenementLogin e)
    {
        Utilisateur.utilisateurCourant=e.getUtilisateur();
        if (e.getUtilisateur()==null)
        {
            codeT.clear();
            codeT.clear();
            motDePasseT.clear();
            message.setText(e.getMessage());
        }
    }

}

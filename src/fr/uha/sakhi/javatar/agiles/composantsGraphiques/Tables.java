package fr.uha.sakhi.javatar.agiles.composantsGraphiques;

import fr.uha.sakhi.javatar.agiles.données.Utilisateurs.*;
import fr.uha.sakhi.javatar.agiles.données.annotations.Colonne;
import fr.uha.sakhi.javatar.agiles.données.annotations.Lister;
import fr.uha.sakhi.javatar.agiles.données.annotations.Modifier;
import fr.uha.sakhi.javatar.agiles.données.annotations.Table;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

public class Tables<T> extends VBox {

    private static  Class DAO;
    private final Supplier<? extends T> ctor;
    private T attribut;
    private String nomTable;
    private String nomAffichageTable;
    private GestionneurBd gestionneurBd;
    private TableView table=new TableView();
    private Label label=new Label();
    ObservableList<T> observableList;
    private Method listage,suppression,chercher,ajouter,modifier;
    private Button confirmerModifications=new Button("Confirmer Modifications");

    /**
     * constructeur
     * @param ctor
     */
    public Tables(Supplier<? extends T> ctor) {
        this.ctor = Objects.requireNonNull(ctor);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                initialiserInformations();
                creerTable();
                ajouterEcouteurs();
            }
        });

    }

    private void ajouterEcouteurs() {
        confirmerModifications.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                for(T t:observableList)
                {
                    appelerMethode(modifier,gestionneurBd,t);
                }
            }
        });
    }

    private void initialiserInformations() {
        if (ctor.get().getClass().isAnnotationPresent(Table.class)) {
            Table annotInstance = ctor.get().getClass().getAnnotation(Table.class);
            DAO =annotInstance.DAO();
            nomTable=annotInstance.nom();
            nomAffichageTable=annotInstance.nomAffichage();
            label.setText(nomAffichageTable);
            if (DAO== UtilisateurDAO.class)
            {
                gestionneurBd= UtilisateurDAO.getInstance();
            }
        }
    }

    public void creerTable() {
        table.setEditable(true);
        attribut = ctor.get();
        creerColonnes();
        creerElements();

    }


    private void creerElements() {
         listage=trouvermethode(DAO, Lister.class);
        //Method suppression=trouvermethode(DAO, Supprimer.class);
        //  Method chercher=trouvermethode(DAO, Chercher.class);
        // Method ajouter=trouvermethode(DAO, Ajouter.class);
         modifier=trouvermethode(DAO, Modifier.class);
         observableList=appelerMethode(listage,gestionneurBd);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {

                table.setItems(observableList);
                table.getColumns().add(creerColonneModifSuppp());
                getChildren().addAll(label);
                getChildren().addAll(confirmerModifications);
                getChildren().addAll(table);
                setSpacing(20);
            }
        });
    }

    private void creerColonnes() {

        final List<Field> attributs = new ArrayList<>(Arrays.asList(ctor.get().getClass().getDeclaredFields()));
        for (final Field attribut : attributs) {
            if (attribut.isAnnotationPresent(Colonne.class)) {
                Colonne annotInstance = attribut.getAnnotation(Colonne.class);
                if(annotInstance.Affichable())
                {
                 table.getColumns().add(creerColonne(attribut,annotInstance));
                }
            }
        }


    }

    private TableColumn<TableColumn, TableCell> creerColonneModifSuppp() {
        TableColumn<TableColumn,TableCell> col = new TableColumn("");
        col.setCellFactory(new Callback<TableColumn<TableColumn, TableCell>, TableCell<TableColumn, TableCell>>() {
            @Override
            public TableCell<TableColumn, TableCell> call(TableColumn<TableColumn, TableCell> param) {
                return new ButtonCelluleModifiable(ctor.get().getClass());

            }
        });
        return col;
    }

    private TableColumn<TableColumn, TableCell> creerColonne(Field attribut, Colonne annotInstance) {
        TableColumn<TableColumn,TableCell> col = new TableColumn(annotInstance.nomDAffichage());
        switch (annotInstance.type())
        {
            case TEXT: {
                col.setCellValueFactory(new PropertyValueFactory(attribut.getName()));
                /*
                if (annotInstance.Modifiable())
                {
                    col.setCellFactory(new Callback<TableColumn<TableColumn, TableCell>, TableCell<TableColumn, TableCell>>() {
                        @Override
                        public TableCell call(TableColumn<TableColumn, TableCell> param) {
                            return new CelluleModifiable();
                        }
                    });
                }
                */
            }

        }
      return col;
    }

    private ObservableList<T> appelerMethode(Method method,GestionneurBd obj,Object ...args)
    {
        ObservableList<T> o=null;
        try {
             o= (ObservableList<T>) method.invoke(obj,args);
        }
        catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException ignored) { }
        catch (NullPointerException e) {
            System.out.println("erreur au niveau des annotaions");
        }
        return o;
    }
    private static Method trouvermethode(final Class<?> cl,final Class Ann) {

        final List<Method> methodes = new ArrayList<>(Arrays.asList(cl.getDeclaredMethods()));

        for (final Method method : methodes) {
            if (method.isAnnotationPresent(Ann)) {
                return method;
                }
            }
        return null;
    }
    }



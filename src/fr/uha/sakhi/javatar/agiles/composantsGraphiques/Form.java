package fr.uha.sakhi.javatar.agiles.composantsGraphiques;
import fr.uha.sakhi.javatar.agiles.données.Utilisateurs.Utilisateur;
import fr.uha.sakhi.javatar.agiles.données.annotations.Colonne;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Popup;
import javafx.util.Pair;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Form extends Popup{
    GridPane gridPane = new GridPane();
     final Class c;
     Object obj;
    Button Confirmer;
    Button annuler;
    TableView tableView;
    public Form(Class c, Object o, TableView<TableColumn> tableView) {
        this.c=c;
        this.obj=o;
        this.tableView=tableView;
        gridPane.setAlignment(Pos.CENTER);

        gridPane.setPadding(new Insets(40, 40, 40, 40));

        gridPane.setHgap(10);

        gridPane.setVgap(10);


        ColumnConstraints columnOneConstraints = new ColumnConstraints(100, 100, Double.MAX_VALUE);
        columnOneConstraints.setHalignment(HPos.RIGHT);

        ColumnConstraints columnTwoConstrains = new ColumnConstraints(200,200, Double.MAX_VALUE);
        columnTwoConstrains.setHgrow(Priority.ALWAYS);

        gridPane.getColumnConstraints().addAll(columnOneConstraints, columnTwoConstrains);

        ajouterElements();
        getContent().add(gridPane);
    }


    private void ajouterElements() {
        Label entete = new Label();
        if (obj==null)
        {
            entete.setText("Ajout");
        }
        else
        {
            entete.setText("Modification");
        }

        entete.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gridPane.add(entete, 0,0,2,1);
        GridPane.setHalignment(entete, HPos.CENTER);
        GridPane.setMargin(entete, new Insets(20, 0,20,0));
        creerElements();

    }

    private void creerElements() {
        int compt=1;
        final List<Field> attributs = new ArrayList<>(Arrays.asList(c.getDeclaredFields()));
        for (final Field attribut : attributs) {
            if (attribut.isAnnotationPresent(Colonne.class)) {
                Colonne annotInstance = attribut.getAnnotation(Colonne.class);
                Pair<Node,Node> p=creerElement(attribut,annotInstance);
                    gridPane.add(p.getKey(),0,compt);
                    gridPane.add(p.getValue(),1,compt);
                    compt++;
            }
        }

        Confirmer = new Button("Confirmer");
        annuler = new Button("Annuler");
        ajouterEcouteurs();
        Confirmer.setPrefHeight(40);
        Confirmer.setDefaultButton(true);
        Confirmer.setPrefWidth(100);
        annuler.setPrefHeight(40);
        annuler.setDefaultButton(true);
        annuler.setPrefWidth(100);
        gridPane.add(Confirmer, 0, compt, 2, 1);
        gridPane.add(annuler, 2, compt, 2, 1);
        GridPane.setHalignment(Confirmer, HPos.CENTER);
        GridPane.setMargin(annuler, new Insets(20, 0,20,0));
    }

    private void ajouterEcouteurs() {
        Confirmer.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

             if (obj!=null)
             {
                 if (obj.getClass()==Utilisateur.class)
                 {
                     ((Utilisateur)obj).setNom(((TextField)getScene().lookup("#nom")).getText());
                     System.out.println(((Utilisateur)obj).getNom());
                     ((Utilisateur)obj).setCode(((TextField)getScene().lookup("#code")).getText());
                     ((Utilisateur)obj).setMotdePasse(((TextField)getScene().lookup("#motdePasse")).getText());
                     ((Utilisateur)obj).setPrenom(((TextField)getScene().lookup("#prenom")).getText());
                     ((Utilisateur)obj).setRole(((TextField)getScene().lookup("#role")).getText());
                     if (tableView!=null)
                     {
                         tableView.refresh();
                     }
                 }
             }
             hide();
            }
        });
        annuler.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            hide();
            }
        });
    }

    private Pair<Node,Node> creerElement(Field attribut, Colonne annotInstance) {
        Pair<Node,Node> p=null;
        if (annotInstance.Modifiable())
        {
            switch (annotInstance.type())
            {
                case TEXT: {
                    TextField text=new TextField();
                    try {

                        if (obj!=null)
                        {
                            if (obj.getClass()==Utilisateur.class)
                            {
                                if(attribut.get(((Utilisateur)obj))!=null)
                                {
                                    text.setText(attribut.get(((Utilisateur)obj)).toString());
                                    text.setId(attribut.getName());
                                }
                            }
                        }

                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    p=new Pair<>(new Label(annotInstance.nomDAffichage()),text);
                }
            }
        }
        return p;
    }



}

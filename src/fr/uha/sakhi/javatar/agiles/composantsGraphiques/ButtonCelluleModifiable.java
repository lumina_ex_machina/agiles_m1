package fr.uha.sakhi.javatar.agiles.composantsGraphiques;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;

import javafx.scene.layout.VBox;

class ButtonCelluleModifiable extends TableCell<TableColumn, TableCell> {

    VBox vBox=new VBox();
    Button modifier=new Button("Modifier");
    Button suprrimer=new Button("Supprimer");
    final Class c;
    public ButtonCelluleModifiable(Class<?> c) {
        this.c=c;
        vBox.getChildren().addAll(modifier,suprrimer);
        modifier.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Form f=new Form(c,getTableRow().getItem(),getTableView());
                f.show(getScene().getWindow());
            }
        });
    }

    @Override
    public void updateIndex(int i) {
        super.updateIndex(i);
        if (  getTableRow()!=null && getTableRow().getItem() !=null)
            setGraphic(vBox);
        else
            setGraphic(null);
    }
}